# 实验4：PL/SQL语言打印杨辉三角

- 学号：201910414403   姓名：程鑫  班级：2020级软件工程4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。



## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

```

调用存储过程：

```sql
set serveroutput on;
declare
   begin
      YHTriange(10); -- 若有参，则加上 "()"
   end;
```

## 结论

1. 在本次实验中，学习了 Oracle PL/SQL 语言以及存储过程的编写。PL/SQL语言是一个开发Oracle数据库应用程序时非常重要的工具。存储过程是一组预编译的PL/SQL代码，用于完成一定的业务逻辑处理功能，并可以通过调用实现。通常情况下，存储过程接收输入参数并返回计算机语言中函数的值或长度。存储过程还可以使用游标、异常处理、动态SQL和外部引用等高级特性扩展其功能。通过本次实验，掌握了Oracle PL/SQL语言及存储过程的编写技巧与方法。PL/SQL语言是进行Oracle数据库开发必不可少的基础工具。
