# 实验3：创建分区表

学号：201910414403 姓名：程鑫 班级：2020级软工4班  

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
创建一个名为orders的数据库表，并对该表进行分区。其中，表中包含了订单id、顾客姓名、顾客电话、订单日期、员工id、折扣、交易应收款项等字段。该表的主键为订单id，分区方式是按照订单日期进行分区。

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```
- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```


执行这段代码会在数据库中创建一个名为SEQ1的序列，可用于生成递增的序列值，其最小值为1，最大值为999999999，每次增加1，从1开始递增，每次缓存20个值。

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```
上述代码是一个PL/SQL脚本，用于向Oracle数据库的orders表中插入100条订单数据，每条订单的日期依次递增，从2015年1月12日开始，每月增加一个月，最后的订单日期为2023年1月12日。
- 在order_details表中插入数据

```sql
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```


## 实验总结
在本次实验中，学习了分区表的创建方法以及各种分区方式的使用场景。分区表是将一张表的数据按照特定的规则在物理上分割成多个部分存储的表。这些部分也被称作分区。每一个分区可以另外指定存放到不同的表空间，方便管理和维护。创建分区表需要使用CREATE TABLE语句，并在语句中指定要进行分区的列、分区方式和分区策略。通过本次实验，了解到分区表的创建方法和分区方式的适用场景，来达到更好地管理数据的目的。
