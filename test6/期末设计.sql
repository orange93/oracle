-- 创建表空间1（TBS1）用于系统表和索引
CREATE TABLESPACE TBS1 DATAFILE 'tbs5_datafile.dbf' SIZE 100M;
-- 创建表空间2（TBS2）用于用户表和数据
CREATE TABLESPACE TBS2 DATAFILE 'tbs6_datafile.dbf' SIZE 2G;
-- 创建产品表（Products）
CREATE TABLE Products (
  ProductID INT PRIMARY KEY,
  ProductName VARCHAR(100),
  Price DECIMAL(10, 2),
  Inventory INT
)TABLESPACE TBS2;
-- 创建客户表（Customers）
CREATE TABLE Customers (
  CustomerID INT PRIMARY KEY,
  Name VARCHAR(100),
  Address VARCHAR(200),
  Contact VARCHAR(50)
)TABLESPACE TBS2;
-- 创建订单表（Orders）
CREATE TABLE Orders (
  OrderID INT PRIMARY KEY,
  CustomerID INT,
  ProductID INT,
  Quantity INT,
  OrderDate DATE,
  FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
  FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
)TABLESPACE TBS2;
-- 创建销售记录表（SalesRecords）
CREATE TABLE SalesRecords (
  RecordID INT PRIMARY KEY,
  OrderID INT,
  SalesDate DATE,
  SalesAmount DECIMAL(10, 2),
  FOREIGN KEY (OrderID) REFERENCES Orders(OrderID)
)TABLESPACE TBS2;

-- 批量插入大量数据到Products表
INSERT INTO Products (ProductID, ProductName, Price, Inventory)
SELECT LEVEL,
       'Product ' || LEVEL,
       LEVEL * 10,
       100
FROM DUAL
CONNECT BY LEVEL <= 100000;
COMMIT;
-- 批量插入大量数据到Customers表
INSERT INTO Customers (CustomerID, Name, Address, Contact)
SELECT LEVEL,
       'Customer ' || LEVEL,
       'Address ' || LEVEL,
       'Contact ' || LEVEL
FROM DUAL
CONNECT BY LEVEL <= 10000;
commit;
--批量插入大量数据到Orders表
INSERT INTO Orders
  (OrderID, CustomerID, ProductID, Quantity, OrderDate)
  SELECT rownum, CustomerID, ProductID, 2, (sysdate - 1)
    FROM Products t, Customers t1
   where rownum <= 20000;
COMMIT;
--批量插入大量数据到SalesRecords表
INSERT INTO SalesRecords
  (RecordID, OrderID, SalesDate, SalesAmount)
  SELECT rownum, OrderID, (sysdate - 1), 100
    FROM Orders t1
   where rownum <= 20000;
COMMIT;

--创建用户和授权(注意创建用户需要DBA账号，即sysdb或sys账号)

-- 创建销售用户（SalesUser）
CREATE USER c##SalesUser IDENTIFIED BY WAAssb123 DEFAULT TABLESPACE TBS2;

-- 授予表1和表2的查询、插入、更新和删除权限给销售用户
GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO c##SalesUser;
GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO c##SalesUser;

-- 创建管理员用户（AdminUser）
CREATE USER c##AdminUser IDENTIFIED BY WAAssb123 DEFAULT TABLESPACE TBS2;

-- 授予所有表的完全权限给管理员用户
GRANT ALL PRIVILEGES ON Products TO c##AdminUser;
GRANT ALL PRIVILEGES ON Customers TO c##AdminUser;
GRANT ALL PRIVILEGES ON Orders TO c##AdminUser;
GRANT ALL PRIVILEGES ON SalesRecords TO c##AdminUser;
--创建包
CREATE OR REPLACE PACKAGE SalesPackage AS
  -- 存储过程：创建订单
  PROCEDURE CreateOrder(
    p_CustomerID IN INT,
    p_ProductID IN INT,
    p_Quantity IN INT
  );

  -- 存储过程：计算销售总额
  PROCEDURE CalculateTotalSales;

  -- 函数：获取客户订单数量
  FUNCTION GetCustomerOrderCount(
    p_CustomerID IN INT
  ) RETURN INT;
END SalesPackage;
CREATE SE

-- 实现存储过程和函数的具体逻辑
CREATE OR REPLACE PACKAGE BODY SalesPackage AS
  -- 存储过程：创建订单
  PROCEDURE CreateOrder(
    p_CustomerID IN INT,
    p_ProductID IN INT,
    p_Quantity IN INT
  ) AS
  BEGIN
    -- 更新产品库存
    UPDATE Products
    SET Inventory = Inventory - p_Quantity
    WHERE ProductID = p_ProductID;
  END CreateOrder;

  -- 存储过程：计算销售总额
  PROCEDURE CalculateTotalSales AS
    total_sales DECIMAL(10, 2);
  BEGIN
    SELECT SUM(SalesAmount) INTO total_sales
    FROM SalesRecords;

    DBMS_OUTPUT.PUT_LINE('Total Sales: ' || total_sales);
  END CalculateTotalSales;

  -- 函数：获取客户订单数量
  FUNCTION GetCustomerOrderCount(
    p_CustomerID IN INT
  ) RETURN INT AS
    order_count INT;
  BEGIN
    SELECT COUNT(*) INTO order_count
    FROM Orders
    WHERE CustomerID = p_CustomerID;

    RETURN order_count;
  END GetCustomerOrderCount;
END SalesPackage;

