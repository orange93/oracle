

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项     | 评分标准                             | 满分 |
| :--------- | :----------------------------------- | :--- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |



### 1.表空间设计方案

1.创建表空间1（TBS1）：用于存储系统表和索引。

```sql
-- 创建表空间1（TBS1）用于系统表和索引
CREATE TABLESPACE TBS1 DATAFILE 'tbs5_datafile.dbf' SIZE 100M;
```
2.创建表空间2（TBS2）：用于存储用户表和数据。

```sql
-- 创建表空间2（TBS2）用于用户表和数据
CREATE TABLESPACE TBS2 DATAFILE 'tbs6_datafile.dbf' SIZE 2G;
```


![](C:\Users\86190\Desktop\test6\p1.png)

- product（商品表）：id（产品ID）name（产品名称）description（产品描述）price（价格）stock（库存）category_id（类别ID）
- customer（顾客表）id（顾客ID）name（顾客姓名）email（电子邮件）phone_number（电话号码）address（地址）
- order（订单表）id（订单ID）customer_id（顾客ID）order_date（下单日期）status（订单状态）
- shipment（发货表）id（发货ID）order_id（订单ID）tracking_number（快递单号）carrier（快递公司）ship_date（发货日期）


### 2.表设计方案：
- 表1：产品表（Products）：产品ID（ProductID）、产品名称（ProductName）、价格（Price）、库存（Inventory）等。
```sql
CREATE TABLE Products (
  ProductID INT PRIMARY KEY,
  ProductName VARCHAR(100),
  Price DECIMAL(10, 2),
  Inventory INT
)TABLESPACE TBS2;
```


![](C:\Users\86190\Desktop\test6\p2-1685111364575-3.png)

- 表2：客户表（Customers）：客户ID（CustomerID）、姓名（Name）、地址（Address）、联系方式（Contact）等。

```sql
CREATE TABLE Customers (
  CustomerID INT PRIMARY KEY,
  Name VARCHAR(100),
  Address VARCHAR(200),
  Contact VARCHAR(50)
)TABLESPACE TBS2;
```



![](C:\Users\86190\Desktop\test6\p3-1685111312749-1.png)

- 表3：订单表（Orders）：订单ID（OrderID）、客户ID（CustomerID）、产品ID（ProductID）、数量（Quantity）、订单日期（OrderDate）等。

```sql
CREATE TABLE Orders (
  OrderID INT PRIMARY KEY,
  CustomerID INT,
  ProductID INT,
  Quantity INT,
  OrderDate DATE,
  FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
  FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
)TABLESPACE TBS2;
```


![](C:\Users\86190\Desktop\test6\p4-1685111395295-5.png)

- 表4：销售记录表（SalesRecords）：销售记录ID（RecordID）、订单ID（OrderID）、销售日期（SalesDate）、销售金额（SalesAmount）等。

```sql
CREATE TABLE SalesRecords (
  RecordID INT PRIMARY KEY,
  OrderID INT,
  SalesDate DATE,
  SalesAmount DECIMAL(10, 2),
  FOREIGN KEY (OrderID) REFERENCES Orders(OrderID)
)TABLESPACE TBS2;
```



![](C:\Users\86190\Desktop\test6\p5.png)

### 插入数据
- 商品表
```sql
INSERT INTO Products (ProductID, ProductName, Price, Inventory)
SELECT LEVEL,
       'Product ' || LEVEL,
       LEVEL * 10,
       100
FROM DUAL
CONNECT BY LEVEL <= 100000;
COMMIT;
```



![](C:\Users\86190\Desktop\test6\p6-1685111421821-7.png)

- 顾客表

```sql
INSERT INTO Customers (CustomerID, Name, Address, Contact)
SELECT LEVEL,
       'Customer ' || LEVEL,
       'Address ' || LEVEL,
       'Contact ' || LEVEL
FROM DUAL
CONNECT BY LEVEL <= 10000;
commit;

```

![](C:\Users\86190\Desktop\test6\p7.png)


- 订单表

```sql
INSERT INTO Orders
  (OrderID, CustomerID, ProductID, Quantity, OrderDate)
  SELECT rownum, CustomerID, ProductID, 2, (sysdate - 1)
    FROM Products t, Customers t1
   where rownum <= 20000;
COMMIT;

```

![](C:\Users\86190\Desktop\test6\p8.png)


- 发货表

```sql
INSERT INTO SalesRecords
  (RecordID, OrderID, SalesDate, SalesAmount)
  SELECT rownum, OrderID, (sysdate - 1), 100
    FROM Orders t1
   where rownum <= 20000;
COMMIT;

```

![](C:\Users\86190\Desktop\test6\p9.png)



### 设计权限及用户分配方案

创建用户，创建管理员用户并授予DBA角色。

```sql
-- 创建销售用户（SalesUser）
CREATE USER c##SalesUser IDENTIFIED BY WAAssb123 DEFAULT TABLESPACE TBS2;

-- 授予表1和表2的查询、插入、更新和删除权限给销售用户
GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO c##SalesUser;
GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO c##SalesUser;

-- 创建管理员用户（AdminUser）
CREATE USER c##AdminUser IDENTIFIED BY WAAssb123 DEFAULT TABLESPACE TBS2;

-- 授予所有表的完全权限给管理员用户
GRANT ALL PRIVILEGES ON Products TO c##AdminUser;
GRANT ALL PRIVILEGES ON Customers TO c##AdminUser;
GRANT ALL PRIVILEGES ON Orders TO c##AdminUser;
GRANT ALL PRIVILEGES ON SalesRecords TO c##AdminUser;
```

![](C:\Users\86190\Desktop\test6\p10.png)



![](C:\Users\86190\Desktop\test6\p11.png)



![](C:\Users\86190\Desktop\test6\p12-1685106789865-16.png)



![](C:\Users\86190\Desktop\test6\p13.png)



### 程序包

```sql
CREATE OR REPLACE PACKAGE SalesPackage AS
  -- 存储过程：创建订单
  PROCEDURE CreateOrder(
    p_CustomerID IN INT,
    p_ProductID IN INT,
    p_Quantity IN INT
  );

  -- 存储过程：计算销售总额
  PROCEDURE CalculateTotalSales;

  -- 函数：获取客户订单数量
  FUNCTION GetCustomerOrderCount(
    p_CustomerID IN INT
  ) RETURN INT;
END SalesPackage;
CREATE SE

-- 实现存储过程和函数的具体逻辑
CREATE OR REPLACE PACKAGE BODY SalesPackage AS
  -- 存储过程：创建订单
  PROCEDURE CreateOrder(
    p_CustomerID IN INT,
    p_ProductID IN INT,
    p_Quantity IN INT
  ) AS
  BEGIN
    -- 更新产品库存
    UPDATE Products
    SET Inventory = Inventory - p_Quantity
    WHERE ProductID = p_ProductID;
  END CreateOrder;

  -- 存储过程：计算销售总额
  PROCEDURE CalculateTotalSales AS
    total_sales DECIMAL(10, 2);
  BEGIN
    SELECT SUM(SalesAmount) INTO total_sales
    FROM SalesRecords;

    DBMS_OUTPUT.PUT_LINE('Total Sales: ' || total_sales);
  END CalculateTotalSales;

  -- 函数：获取客户订单数量
  FUNCTION GetCustomerOrderCount(
    p_CustomerID IN INT
  ) RETURN INT AS
    order_count INT;
  BEGIN
    SELECT COUNT(*) INTO order_count
    FROM Orders
    WHERE CustomerID = p_CustomerID;

    RETURN order_count;
  END GetCustomerOrderCount;
END SalesPackage;
```



![](C:\Users\86190\Desktop\test6\p14.png)



![](C:\Users\86190\Desktop\test6\p15-1685107357418-21.png)



## 备份方案

首先，定期进行完整数据库备份是一种重要的数据保护措施，可以确保在出现系统故障、数据丢失等问题时可以快速恢复数据。除了数据文件外，还应该备份控制文件和日志文件，以便数据库管理人员可以使用这些信息进行恢复操作。一般建议每天至少进行一次完整备份，特别是对于关键数据和应用场景。

其次，可以使用Oracle的备份和恢复工具（如RMAN）进行备份操作。RMAN被广泛用于Oracle数据库的备份和恢复。它支持完整备份、增量备份、归档备份、压缩备份等不同类型备份方式，可提供灵活的备份和恢复策略。

第三，将备份数据存储在独立的存储介质上也很重要，以保证数据的安全性和可靠性。如果仅在当前系统存储备份数据，当系统遭受硬件故障或病毒攻击等情况时会丢失备份数据，因此需要选择可靠的独立存储介质（如磁带库、云存储等）来存储备份数据。

最后，增量备份策略是一种有效的方法，可以减少备份时间和存储空间的需求。增量备份只备份发生更改的数据块和日志信息，而不会每次备份完整数据库，因此备份时间和存储空间要比完整备份少得多。但需要注意的是，在使用增量备份时，必须保证之前的最近一次完整备份和归档日志能够恢复原始数据。

## 

## 实验总结

本次实验主要涉及到了表空间设计、表设计、权限及用户分配方案、PL/SQL程序包以及数据库备份方案等方面的内容:
1.表空间设计方案：在本次实验中，创建了两个数据表空间，TBS1用于存储系统表和索引，TBS2用于存储用户表和数据。分离系统表和用户表的目的是提高数据库的可维护性和安全性。
2.表设计方案：本次实验中，设计了四张数据表：产品表（Products）、客户表（Customers）、订单表（Orders）和销售记录表（SalesRecords）。这些表包括了典型的业务场景，其中涉及到的数据都是比较常见的。在实际应用场景中，如果需要根据具体的业务需求来设计新的表，也可以参考这些表的设计思路。
3.权限及用户分配方案：安全性是数据库设计考虑的重要因素之一。在本次实验中，创建了两个用户，分别为管理员用户（AdminUser）和销售用户（SalesUser），并授予了相应的表操作权限。通过这种灵活的权限管理方式，可以保证不同类型的用户只能访问相应的数据，提高了数据库的安全性。
4.PL/SQL程序包：PL/SQL是Oracle数据库中的一种编程语言，可以在数据库中实现更加复杂的业务逻辑。在本次实验中，创建了一个程序包SalesPackage，其中包含三个子程序：CreateOrder、CalculateTotalSales和GetCustomerOrderCount。通过调用这些子程序，可以快速地进行各种业务操作，例如新建订单、计算销售总额以及获取特定客户的订单数量等。通过这些存储过程和函数，可以简化部分SQL代码、提高效率以及减少出错的可能性。
5.数据库备份方案：在数据库备份方面，可以定期进行完整数据库备份，使用Oracle的RMAN等工具进行数据库备份管理，以确保数据库数据的安全性和可靠性，同时考虑增量备份策略，以便更好地节约时间和存储空间。
本次实验让我更加深入地了解了构建数据库系统的过程和思路，从概念阶段到操作阶段，包括表空间设计、表设计、权限和用户分配、PL/SQL程序包以及备份方案等多个方面展开。在这个过程中，我学习了如何用合理和正确的方式对数据进行组织、存取、处理、安全保障和备份恢复，在现实应用场景下建立一个数据库系统。
