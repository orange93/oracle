# 实验5：包，过程，函数的用法

程鑫   201910414403

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录
  1. 创建一个包(Package)，包名是MyPack。
  2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
  3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

```
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](C:\Users\86190\Desktop\test5\Pict1-1685100998374-1.png)
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID， FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 测试

```sh
函数Get_SalaryAmount()测试：
SQL> select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4400
	   20 Marketing 			    19000
	   30 Purchasing			    24900
	   40 Human Resources			     6500
	   50 Shipping				   156400
	   60 IT				    28800
	   70 Public Relations			    10000
	   80 Sales				   304500
	   90 Executive 			    58000
	  100 Finance				    51608
	  110 Accounting			    20308

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support


```

![pict2](pict2.png)

## 总结

实验总结

在本次实验中，主要学习了PL/SQL语言结构、变量和常量的声明及使用方法，以及包、过程和函数的用法。PL/SQL是Oracle数据库提供的一种标准的过程化编程语言，变量和常量是PL/SQL编程中非常重要的元素。它们被用于存储和处理数据。包是一个由附加到数据库的函数、过程及其相关变量和常量等的程序单元，用于封装和组织PL/SQL程序代码。包通过封装逻辑相似的程序来提高程序的可读性、复用性，同时也更容易进行维护。本次实验基本掌握了PL/SQL语言结构、变量和常量的声明及使用方法，掌握了包、过程和函数的用法。

